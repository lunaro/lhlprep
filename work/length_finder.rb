def length_finder(input_array)
  input_array.collect{|string|string.length}
end